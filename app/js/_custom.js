document.addEventListener("DOMContentLoaded", function() {

    $('select').niceSelect();

    // не знаю, надо ли было, но можно такое решение примерно для позиции крестика х)
    function resetIconPosition() {
        const inputText = $('.search-input__query').val().length + 1;
        const position = 56 + parseInt(inputText) * 8.3 + 10;
        console.log(inputText);
        $('.search-input__reset').css('left', position + 'px');
    }

    resetIconPosition();

    $('.search-input__query').on('keydown', () => {
       resetIconPosition();
    });

    // Нашел код под range
    (function() {
        function rangeInputChangeEventHandler(e){
            let rangeGroup = $(this).attr('name'),
                minBtn = $('.min'),
                maxBtn = $('.max'),
                range_min = $('.range-min'),
                range_max = $('.range-max'),
                minVal = parseInt($(minBtn).val()),
                maxVal = parseInt($(maxBtn).val());
            minVal = parseInt($(minBtn).val());
            $(range_min).val(minVal);
            maxVal = parseInt($(maxBtn).val());
            $(range_max).val(maxVal);
        }
        $('input[type="range"]').on( 'input', rangeInputChangeEventHandler);
    })();


});
